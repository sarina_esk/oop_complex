TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp \
    complex.cpp \
    menu.cpp

HEADERS += \
    complex.h \
    menu.h
