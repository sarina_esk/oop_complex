#ifndef COMPLEX_H
#define COMPLEX_H
#include<iostream>
using namespace std;

class Complex
{
private:
    float real;
    float image;

public:
    Complex();
    void read();
    void display();
    Complex operator + (Complex co2);
    Complex operator - (Complex co2);
    Complex operator * (Complex co2);
    Complex operator / (Complex co2);
    bool operator ==(Complex co2);
    float getReal();
    float getImage();

    void show();


};

#endif // COMPLEX_H
